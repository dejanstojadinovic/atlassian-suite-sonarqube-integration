/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import com.marvelution.atlassian.suite.plugins.sonarqube.ApplinksPlugin;
import org.sonar.api.web.ServletFilter;

import javax.servlet.*;
import java.io.IOException;

/**
 * {@link ServletFilter} to filter for the Application Links authentication configuration requests
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplinksAuthConfServletFilter extends ServletFilter {

	private static final String AUTH_CONF_URL = "/plugins/servlet/applinks/auth/conf/";
	private ServletContext servletContext;

	@Override
	public UrlPattern doGetPattern() {
		return UrlPattern.create(AUTH_CONF_URL + "*");
	}

	/**
	 * See {@link javax.servlet.Filter#init(javax.servlet.FilterConfig)}
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		servletContext = filterConfig.getServletContext();
	}

	/**
	 * See {@link javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)}
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/static/" + ApplinksPlugin.getManifest().getKey() +
				"/authConf.html");
		dispatcher.forward(request, response);
	}

	/**
	 * See {@link javax.servlet.Filter#destroy()}
	 */
	public void destroy() {
	}

}
