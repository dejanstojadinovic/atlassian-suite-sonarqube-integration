#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class StreamFeed

  attr_reader :link, :title, :updated, :entries

  def initialize(link, title, updated = Time.now)
    @link = link
    @title = title
    @updated = updated
    @entries = []
  end

  def add_entry(entry)
    @entries.push(entry) unless entry.nil?
  end

  def sort_and_limit(size)
    @entries = @entries.sort_by { |a| a.published }.reverse!
    @entries = @entries.take(size) if @entries.length > size
  end

end
