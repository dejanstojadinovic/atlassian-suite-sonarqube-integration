#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class WidgetConfigController < ApplicationController

  helper :dashboard

  SECTION = Navigation::SECTION_RESOURCE

  def index
    widget_key = params[:id]
    @widget_definition = java_facade.getWidget(widget_key)
    if @widget_definition
      if @widget_definition.getWidgetProperties().empty?
        render :status => 200, :text => ''
      else
        @widget = Widget.new(:widget_key => widget_key)
        @widget.id = 1
        @widget_definition.getWidgetProperties().each do |property_definition|
          value = params[property_definition.key()]
          @widget.properties << WidgetProperty.new(
              :widget => @widget,
              :kee => property_definition.key(),
              :text_value => (value.blank? ? property_definition.defaultValue : value)
          )
        end
        params[:layout] = 'false'
        render :action => 'index'
      end
    else
      render :status => 404, :text => "Unknown Widget #{widget_key}"
    end
  end

end
