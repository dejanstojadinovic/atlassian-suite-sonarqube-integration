#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# API implementation of the Applinks /rest/applinks/1.0/ REST resources
class Api::ApplinksController < Api::ApiController

  include ApplinksHelper

  skip_before_filter :check_authentication, :only => [:manifest, :permission, :entities]
  before_filter :admin_required, :only => [:authenticationinfo, :applicationlink, :entitylink, :status]

  # GET /api/applinks/manifest
  def manifest
    manifest = Manifest.new(applinks_id(), applinks_name)
    manifest.version = sonar_version
    manifest.applinks_version = applinks_version
    manifest.public_signup = Property.value('sonar.allowUsersToSignUp')
    manifest.url = base_url
    manifest.icon_url = base_url + url_for_static(:plugin => plugin_key, :path => 'images/icon16_sonarqube.png')
    render :xml => manifest.to_xml()
  end

  # GET /api/applinks/permission
  def permission
    permission = PermissionCode.new(has_role?(:admin))
    render :xml => permission.to_xml()
  end

  # GET /api/applinks/entities
  def entities
    entities = Entities.new
    load_projects.each do |project|
      entity = Entity.new(project.key, project.name(true))
      entity.icon_url = base_url + url_for_static(:plugin => plugin_key, :path => 'images/icon16_sonarqube.resource.png')
      entities.add_entity(entity)
    end
    render :xml => entities.to_xml()
  end

  # GET /api/applinks/authenticationinfo/id/[id]/url/[url]
  def authenticationinfo
    matches = /^(.*)\/id\/(.*)\/url\/([^?]*)/.match(request.fullpath)
    if matches.nil?
      render :text => '', :status => :ok
    else
      if verify_remote_application_id(matches[2], matches[3])
        render :text => '', :status => :ok
      else
        render :text => '', :status => :not_found
      end
    end
  end

  # PUT/DELETE /api/applinks/applicationlink/[id]
  def applicationlink
    matches = /(.*)\/applicationlink\/([^?]*)/.match(request.fullpath)
    if matches.nil?
      render :text => '', :status => :bad_request
    else
      if request.put?()
        xml = Hash.from_xml(request.body)
        ApplicationLink.create!(
            :application_id => xml['applicationLink']['id'],
            :type_id => xml['applicationLink']['typeId'],
            :name => xml['applicationLink']['name'],
            :display_url => xml['applicationLink']['displayUrl'],
            :rpc_url => xml['applicationLink']['rpcUrl'],
            :icon_url => xml['applicationLink']['iconUrl'],
            :primare => xml['applicationLink']['isPrimary'],
            :system => xml['applicationLink']['isSystem']
        )
        render :text => '', :status => :created
      elsif request.delete?()
        ApplicationLink.clear(matches[2])
        render :text => '', :status => :ok
      else
        render :text => '', :status => :bad_request
      end
    end
  end

  # PUT /api/applinks/entitylink/[entity type]/[resource id or key]
  # DELETE  /api/applinks/entitylink/[entity type]/[resource id or key]?typeId=[type id]&key=[key]&applicationId=[application id]
  def entitylink
    matches = /(.*)\/entitylink\/(.*)\/([^?]*)/.match(request.fullpath)
    if matches.nil?
      render :text => '', :status => :bad_request
    else
      if request.put?()
        xml = Hash.from_xml(request.body)
        EntityLink.create!(
            :project_id => Project.by_key(matches[3]).id,
            :application_id => xml['entityLink']['applicationId'],
            :type_id => xml['entityLink']['typeId'],
            :name => xml['entityLink']['name'],
            :kee => xml['entityLink']['key'],
            :primare => xml['entityLink']['isPrimary']
        )
        render :text => '', :status => :created
      elsif request.delete?()
        EntityLink.clear(Project.by_key(matches[3]).id, params[:typeId], params[:key], params[:applicationId])
        render :text => '', :status => :ok
      else
        render :text => '', :status => :bad_request
      end
    end
  end

  # GET /api/applinks/status
  def status
    xml = Builder::XmlMarkup.new(:indent => 0)
    xml.instruct!
    xml.applicationLinks do
      ApplicationLink.find(:all).each do |link|
        link.to_xml(xml)
      end
    end
    render :xml => xml.target!
  end

end
