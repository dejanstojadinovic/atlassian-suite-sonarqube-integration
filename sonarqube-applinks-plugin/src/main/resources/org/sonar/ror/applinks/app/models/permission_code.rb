#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class PermissionCode

  attr_reader :code

  def initialize(allowed = false)
    if allowed
      @code = 'ALLOWED'
    else
      @code = 'NO_PERMISSION'
    end
  end

  def to_xml(xml = Builder::XmlMarkup.new(:indent => 0))
    xml.instruct!
    xml.permissionCodeEntity do
      xml.code(@code)
    end
  end

end