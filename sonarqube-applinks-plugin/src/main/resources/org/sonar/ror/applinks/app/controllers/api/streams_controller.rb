#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# API implementation of the Activity Streams /rest/activity-stream/1.0/ REST resources
class Api::StreamsController < Api::ApiController

  include StreamsHelper

  RESOURCE_KEY = 'key'
  CATEGORY_KEY = 'category'
  USER_KEY = 'user'
  IS_OPERATOR = {:key => 'is', :name => 'is'}
  NOT_OPERATOR = {:key => 'not', :name => 'is not'}

  skip_before_filter :check_authentication, :only => [:config]

  # GET /api/streams/config?local=[local]
  def config
    resources = {}
    load_projects_no_auth_check.each do |project|
      resources[project.key] = project.name(true)
    end
    categories = {}
    EventCategory.categories(true).each do |category|
      categories[category.name] = category.name
    end
    filters = {:filters => [{
                                :applinkName => '',
                                :key => 'streams',
                                :name => '',
                                :options => [{
                                                 :key => RESOURCE_KEY,
                                                 :name => message('applinks.streams.filter.resource.name'),
                                                 :helpText => message('applinks.streams.filter.resource.help.text'),
                                                 :operators => [IS_OPERATOR, NOT_OPERATOR],
                                                 :type => 'select',
                                                 :unique => true,
                                                 :values => resources
                                             }, {
                                                 :key => USER_KEY,
                                                 :name => message('applinks.streams.filter.user.name'),
                                                 :helpText => message('applinks.streams.filter.user.help.text'),
                                                 :operators => [IS_OPERATOR, NOT_OPERATOR],
                                                 :type => 'user',
                                                 :unique => true,
                                                 :values => {}
                                             }
                                ]
                            }, {
                                :applinkName => '',
                                :key => 'sonarqube',
                                :name => message('applinks.streams.filter.name'),
                                :options => [{
                                                 :key => CATEGORY_KEY,
                                                 :name => message('applinks.streams.filter.category.name'),
                                                 :helpText => message('applinks.streams.filter.category.help.text'),
                                                 :operators => [IS_OPERATOR, NOT_OPERATOR],
                                                 :type => 'select',
                                                 :unique => true,
                                                 :values => categories
                                             }]
                            }]
    }
    render :json => filters, :content_type => 'application/vnd.atl.streams+json'
  end

  # GET /api/streams/feed?streams[]=[streams filter]&sonarqube[]=[sonar filter]&maxResutls=[max results]&local=[local]
  def feed
    max_results = params[:maxResults].to_i
    filters = {
        RESOURCE_KEY => {:operator => IS_OPERATOR[:key], :keys => nil}
    }
    unless params[:streams].nil?
      params[:streams].each do |filter|
        parts = filter.split(' ')
        operator = parts[1].downcase
        filters[parts[0]] = {:operator => operator, :keys => parts.drop(2)}
      end
    end
    unless params[:sonarqube].nil?
      params[:sonarqube].each do |filter|
        parts = filter.split(' ')
        operator = parts[1].downcase
        filters[parts[0]] = {:operator => operator, :keys => parts.drop(2)}
      end
    end

    projects = {:ids => [], :keys => []}
    load_projects(filters[RESOURCE_KEY][:keys], filters[RESOURCE_KEY][:operator].eql?(IS_OPERATOR[:key])).each do |project|
      projects[:ids] << project.id
      projects[:keys] << project.key
    end

    feed = StreamFeed.new(request.url, message('applinks.streams.feed.name', :params => [applinks_name]))

    if filters[USER_KEY].nil? or filters[USER_KEY][:operator].eql?(NOT_OPERATOR[:key])
      event_conditions = ['resource_id IN (:rids)']
      event_values = {:rids => projects[:ids]}
      unless filters[CATEGORY_KEY].nil?
        if filters[CATEGORY_KEY][:operator].eql?(IS_OPERATOR[:key])
          event_conditions << 'category IN (:categories)'
        else
          event_conditions << 'category NOT IN (:categories)'
        end
        event_values[:categories] = filters[CATEGORY_KEY][:keys]
      end
      events = Event.find(:all, :conditions => [event_conditions.join(' AND '), event_values], :order => 'event_date DESC',
                          :limit => max_results)
      unless events.nil?
        events.each do |event|
          entry = get_entry_from_event(event)
          feed.add_entry(entry) unless entry.nil?
        end
      end
    end

    issues_filter = {
        :componentRoots => projects[:keys],
        :sort => 'UPDATE_DATE',
        :asc => false,
        :pageSize => max_results
    }
    issues_filter_result = Api.issues.find(issues_filter)
    issues_filter_result.issues.each_with_index do |issue, index|
      break unless index < max_results
      reporter = issues_filter_result.user(issue.reporter)
      if user_allowed_by_filter(reporter, filters[USER_KEY])
        entry = get_entry_from_issue(issue, reporter)
        feed.add_entry(entry) unless entry.nil?
      end
      issue.comments.each do |comment|
        comment_user = issues_filter_result.user(comment.userLogin())
        if user_allowed_by_filter(comment_user, filters[USER_KEY])
          entry = get_entry_from_issue_comment(issue, comment, comment_user)
          feed.add_entry(entry) unless entry.nil?
        end
      end
      changelog = Internal.issues.changelog(issue.key)
      changelog.changes.each_with_index do |change, i|
        break unless i < max_results
        changelog_user = changelog.user(change)
        if user_allowed_by_filter(changelog_user, filters[USER_KEY])
          entry = get_entry_from_issue_changelog(issue, change, changelog_user)
          feed.add_entry(entry) unless entry.nil?
        end
      end
    end

    feed.sort_and_limit(max_results)
    render :xml => feed_to_xml(feed), :content_type => 'application/atom+xml'
  end

end
