This project features plugins to integrate SonarQube with Confluence, Bamboo and JIRA
More details on <https://marvelution.atlassian.net/wiki/display/SIFAS>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/SIFAS>

Continuous Builder
==================
<https://marvelution.atlassian.net/builds/browse/SIFAS>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
