/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.namespace("SonarQubeWidget");


SonarQubeWidget.defaults = {
	baseUrl: AJS.contextPath || "",
	key: "",
	entity: "",
	remoteEntity: "",
	remoteApplication: "",
	width: "",
	properties: undefined,
	propertiesChanged: undefined,
	editable: true,
	frameload: undefined
};

SonarQubeWidget.template = function (options) {
	options = AJS.$.extend({}, SonarQubeWidget.defaults, options);
	var div = AJS.$(SonarQubeTemplate.widget.div(options));
	AJS.$("iframe.view", div).on("load", function () {
		var $iframe = AJS.$(this);
		AJS.log("Loaded widget, key: " + options.key);
		$iframe.height($iframe.contents().find("html").height());
		if (options.frameload) {
			options.frameload.apply(null, [$iframe.contents().find("title").text()]);
		}
	});
	if (options.editable) {
		AJS.$("iframe.config", div).on("load", function () {
			var $container = SonarQubeWidget.getContainer(this);
			var $iframe = AJS.$(this);
			if ($iframe.contents().find(":input").length == 0) {
				AJS.log("Widget " + options.key + " is not configurable! Removing edit-form and widget-menu");
				AJS.$(".widget-menu", $container).remove();
				AJS.$(".edit-form", $container).remove();
			} else {
				$iframe.height($iframe.contents().find("html").height() * 1.75);
			}
		});
		AJS.$(".aui-iconfont-configure", div).click(function () {
			var $container = SonarQubeWidget.getContainer(this);
			SonarQubeWidget.toggleView($container);
			if (options.frameload) {
				options.frameload.apply(null, [AJS.$("iframe.config", $container).contents().find("title").text()]);
			}
		});
		AJS.$("#save", div).click(function () {
			var $container = SonarQubeWidget.getContainer(this);
			options.properties = [];
			AJS.$.each(AJS.$("iframe.config", $container).contents().find(":input"), function () {
				var property = SonarQubeWidget.inputToProperty(this);
				if (property) {
					options.properties.push(property);
				}
			});
			AJS.$("iframe.view", $container).attr("src", SonarQubeTemplate.widget.url(AJS.$.extend({action: "view"}, options)));
			AJS.$("iframe.config", $container).attr("src", SonarQubeTemplate.widget.url(AJS.$.extend({action: "config"}, options)));
			SonarQubeWidget.toggleView($container);
			if (options.propertiesChanged) {
				options.propertiesChanged.apply(null, [options.properties]);
			}
		});
		AJS.$("#cancel", div).click(function () {
			var $container = SonarQubeWidget.getContainer(this);
			SonarQubeWidget.toggleView($container);
			if (options.frameload) {
				options.frameload.apply(null, [AJS.$("iframe.view", $container).contents().find("title").text()]);
			}
		});
	}
	return div;
};

SonarQubeWidget.toggleView = function (container) {
	var view = AJS.$(".view", container);
	var config = AJS.$(".edit-form", container);
	if (view.hasClass("hidden")) {
		view.removeClass("hidden");
		config.addClass("hidden");
	} else {
		view.addClass("hidden");
		config.removeClass("hidden");
	}
}

SonarQubeWidget.getContainer = function (element) {
	return AJS.$(element).closest(".sonarqube-widget");
};

SonarQubeWidget.inputToProperty = function (input) {
	input = AJS.$(input);
	if (input.attr("name")) {
		return {
			key: input.attr("name"),
			value: input.val()
		};
	} else {
		return null;
	}
};
