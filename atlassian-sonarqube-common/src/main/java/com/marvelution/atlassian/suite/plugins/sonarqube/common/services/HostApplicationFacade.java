/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services;

import com.atlassian.applinks.api.*;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReference;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences;

import javax.annotation.Nullable;

/**
 * Local Application Links types provider
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface HostApplicationFacade {

	/**
	 * Get the local {@link ApplicationType}
	 *
	 * @return the local {@link ApplicationType}
	 */
	ApplicationType getApplicationType();

	/**
	 * Get the {@link ApplicationLink} for the given {@link ApplicationId}
	 *
	 * @param applicationId the {@link ApplicationId}
	 * @return the {@link ApplicationLink}
	 */
	ApplicationLink getApplicationLink(ApplicationId applicationId);

	/**
	 * Get the local {@link EntityType}
	 *
	 * @return the local {@link EntityType}
	 */
	EntityType getEntityType();

	/**
	 * Get a local Entity by its key
	 *
	 * @param key the ley of the local entity
	 * @return the local {@link EntityReference}
	 */
	EntityReference getEntity(String key);

	/**
	 * Get all the local Entities
	 *
	 * @return the local {@link EntityReferences}
	 */
	EntityReferences getEntities();

	/**
	 * Get the primary {@link EntityLink} of a local entity
	 *
	 * @param localKey      the local entity key
	 * @return the {@link EntityLink}, may be {@code null}
	 */
	EntityLink getEntityLink(String localKey);

	/**
	 * Get an {@link EntityLink}
	 *
	 * @param localKey      the local entity key
	 * @param applicationId the remote {@link ApplicationId}
	 * @param remoteKey     the remote entity key
	 * @return the {@link EntityLink}, may be {@code null}
	 */
	EntityLink getEntityLink(String localKey, @Nullable ApplicationId applicationId, @Nullable String remoteKey);

	/**
	 * Get all {@link EntityLink}s of a local entity
	 *
	 * @param localKey      the local entity key
	 * @param applicationId the remote {@link ApplicationId} to filter by, may be {@code null}
	 * @return collection of {@link EntityLink}s
	 */
	Iterable<EntityLink> getEntityLinks(String localKey, @Nullable ApplicationId applicationId);

	/**
	 * Get if there is a current authenticated user logged in
	 *
	 * @return {@code true} if there is a current logged in user, {@code false} otherwise
	 */
	boolean isAuthenticated();

	/**
	 * Get if the current authenticated user is a system administrator
	 *
	 * @return {@code true} if the current user is a system administrator, {@code false} otherwise
	 */
	boolean isSystemAdministrator();

}
