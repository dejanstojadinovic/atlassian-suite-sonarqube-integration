/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestProducer;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.sal.api.net.*;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.httpclient.HttpStatus;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * AppLinksManifestProducer for SonarQube applications
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class SonarQubeManifestProducer implements ManifestProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SonarQubeManifestProducer.class);
	private static final int CONNECTION_TIMEOUT = 10000;
	private final RequestFactory<Request<Request<?, Response>, Response>> requestFactory;
	private final ManifestRetriever manifestRetriever;

	/**
	 * Constructor
	 *
	 * @param requestFactory the {@link com.atlassian.sal.api.net.RequestFactory}
	 * @param manifestRetriever the {@link com.atlassian.applinks.spi.manifest.ManifestRetriever}
	 */
	protected SonarQubeManifestProducer(RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
	                                    ManifestRetriever manifestRetriever) {
		this.requestFactory = requestFactory;
		this.manifestRetriever = manifestRetriever;
	}

	@Override
	public Manifest getManifest(final URI url) throws ManifestNotFoundException {
		try {
			return manifestRetriever.getManifest(url);
		} catch (ManifestNotFoundException e) {
			LOGGER.warn("Unable to get the manifest for {}", url);
			return createManifest(url);
		}
	}

	/**
	 * Create a {@link com.atlassian.applinks.spi.Manifest} for the {@link java.net.URI} given
	 *
	 * @param url the {@link java.net.URI} to create a {@link com.atlassian.applinks.spi.Manifest} for
	 * @return the created {@link com.atlassian.applinks.spi.Manifest}
	 */
	protected Manifest createManifest(final URI url) {
		return new Manifest() {

			@Override
			public ApplicationId getId() {
				return ApplicationIdUtil.generate(url);
			}

			@Override
			public String getName() {
				return "SonarQube";
			}

			@Override
			public TypeId getTypeId() {
				return SonarQubeApplicationType.TYPE_ID;
			}

			@Override
			public String getVersion() {
				return null;
			}

			@Override
			public Long getBuildNumber() {
				return 0L;
			}

			@Override
			public URI getUrl() {
				return url;
			}

			@Override
			public Version getAppLinksVersion() {
				return null;
			}

			@Override
			public Boolean hasPublicSignup() {
				return null;
			}

			@Override
			public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
				return ImmutableSet.<Class<? extends AuthenticationProvider>>of(BasicAuthenticationProvider.class);
			}

			@Override
			public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
				return Collections.emptySet();
			}

		};
	}

	@Override
	public ApplicationStatus getStatus(URI url) {
		try {
			LOGGER.debug("Querying " + url + " for its online status.");
			final Request<Request<?, Response>, Response> request =
					requestFactory.createRequest(Request.MethodType.GET, url.toString());
			request.setConnectionTimeout(CONNECTION_TIMEOUT).setSoTimeout(CONNECTION_TIMEOUT);
			return request.executeAndReturn(new ReturningResponseHandler<Response, ApplicationStatus>() {
				@Override
				public ApplicationStatus handle(final Response response) throws ResponseException {
					return response.isSuccessful() || (response.getStatusCode() == HttpStatus.SC_FORBIDDEN) ?
							ApplicationStatus.AVAILABLE : ApplicationStatus.UNAVAILABLE;
				}

			});
		} catch (ResponseException re) {
			return ApplicationStatus.UNAVAILABLE;
		}
	}

}
