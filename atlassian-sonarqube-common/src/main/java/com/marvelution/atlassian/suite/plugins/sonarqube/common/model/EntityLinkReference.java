/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity Reference
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement(name = "entitylink-reference")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntityLinkReference {

	public final String key;
	public final String name;
	public final String applicationId;
	public final boolean primary;

	/**
	 * Constructor
	 *
	 * @param key           the entity key
	 * @param name          the entity name
	 * @param applicationId the applicationId
	 * @param primary       the primary flag
	 */
	public EntityLinkReference(String key, String name, String applicationId, boolean primary) {
		this.key = key;
		this.name = name;
		this.applicationId = applicationId;
		this.primary = primary;
	}

}
